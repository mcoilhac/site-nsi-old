def reaction(vitesse):
    """
    Cette fonction renvoie la distance de réaction pour une
    certaine valeur de vitesse
    Par exemple reaction(50) doit renvoyer 13.888888....
    """
    ...

def freinage(vitesse):
    """
    Cette fonction renvoie la distance de freinage pour une
    certaine valeur de vitesse
    Par exemple freinage(50) doit renvoyer 12.5
    """
    ...

def arret(vitesse):
    """
    Cette fonction renvoie la distance totale d arret pour une
    certaine valeur de vitesse
    Par exemple arret(50) doit renvoyer 26.388888....
    """
    ...

vitesse = float(input("Quelle est votre vitesse en km/h : "))
...






# Test
# Vos affichages ci-dessous
