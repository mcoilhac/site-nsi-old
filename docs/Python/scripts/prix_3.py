def prix_etudiants(nbre_adultes, nbre_enfants, nbre_etudiants):
    ...

adultes = int(input("nombre d'adultes ? "))
enfants = int(input("nombre d'enfants ? "))
etudiants = int(input("nombre d'étudiants ? "))
a_payer = prix_etudiants(adultes, enfants, etudiants)
print("A payer : ", a_payer)

# Tests
print("pour 1 adulte, 3 enfants et 2 étudiants : ", prix_etudiants(1, 3, 2))

