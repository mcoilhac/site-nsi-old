# Import de la fonction randint :
from random import randint

# L'ordinateur "choisit" un nombre  entre 1 et 10 :
nombre_cache = randint(1, 10)

# On commence une boucle qui continue tant que on a pas trouvé ou abandonné le jeu

# Au départ : on n'a pas trouvé (on n'a même rien proposé ...)
continuer = True
while continuer: # Observez qu'il est inutile d'écrire continuer == True car continuer est booléen
    entree = input("entrez un nombre entre 1 et 10 (ou q pour quitter) :" )
    if entree == "q" :
        continuer = False
        print("Abandon !")
    elif int(entree) == nombre_cache :
        continuer = False
        print("Bravo !")
    else :
        print("ce n'est pas la bonne réponse") # Le drapeau est resté évalué à True,
                                               # on refait un tour de boucle

