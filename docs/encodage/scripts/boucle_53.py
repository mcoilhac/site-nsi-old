x = 1.0
y = x + 1.0
while y != x:
    x = x * 2
    y = x + 1.0

# En sortie de boucle
print("En sortie de boucle : ")
print("x = ", x)
print("y = ", y)
print("y - x = ", y - x)


