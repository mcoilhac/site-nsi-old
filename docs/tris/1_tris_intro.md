---
author: Valérie Mousseaux et Mireille Coilhac
title: Tris - introduction
---

## I. Trier

???+ note dépliée

    Pourquoi trie-t-on ?

    ![pourquoi](images/pourquoi.jpg){ width=40%; : .center }


* Dans la vie courante, les deux verbes trier et classer ne sont pas synonymes.
Trier ou effectuer un tri c’est répartir les éléments en paquets correspondant à un certain critère : par exemple séparer les déchets selon leur nature, les personnes d’une assemblée selon leur sexe ou selon leur langue maternelle.

![tri1](images/tri.jpg){ width=80%; : .center }

* Classer ou effectuer un classement c’est mettre des éléments selon un certain ordre : par exemple ranger les personnes d’une assemblée de la plus petite à la plus grande, ou de la plus jeune à la plus âgée.

![tri2](images/pokemon.jpg){ width=80%; : .center }

!!! info "Trier"

    En informatique les mots tri et trier sont à prendre avec le sens de classement et classer.

## II. Imaginons des algorithmes de tris

Le lien suivant va nous aider à imaginer des algorithmes de tris en manipulant des cartes :

[Simulateur de jeu de cartes](https://deck.of.cards/){ .md-button target="_blank" rel="noopener" }


💡 Vous devez imaginer et expliquer une méthode qui permette de trier des cartes. Il y a beaucoup de méthodes possibles ! A vous d'en trouver au moins une !

🧱 Nous allons étudier cette année deux algorithmes de tris. Vous devrez mémoriser ces deux algorithmes.


## III. Le tri `sorted` natif en Python

???+ question "Comment trier une liste, si elle n'est pas triée ?"

    👉 En Python, vous pourrez utiliser la fonction `sorted`

    Tester ci-dessous


    {{ IDE('scripts/help_sorted') }}


???+ question "Tester la fonction `sorted`"

    {{ IDE('scripts/tri_hello') }}


???+ question "Tri décroissant avec `sorted`"

    Tester ci-dessous

    {{ IDE('scripts/reverse') }}


## Crédits

Auteurs : Mireille COILHAC, Valérie MOUSSEAUX et Jean-Louis THIROT





