---
author: Fabrice Nativel et Mireille Coilhac
title: Exercices
---

## I. QCM final

???+ question "1. Entre quelles balises se trouvent le contenu d'un fichier html qui s'affiche dans un navigateur ?"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] `<html>` et  `</html>`
        - [ ] `<head>` et `</head>`
        - [ ] `<title>` et `</title>`
        - [ ] `<body>` et `</body>`

    === "Solution"
        
        - :x: 
        - :x:
        - :x:
        - :white_check_mark:


???+ question "2. Quelle balise html permet d'insérer un lien hypertexte ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `<a>`
        - [ ] `<p>`
        - [ ] `<link>`
        - [ ] `<href>`

    === "Solution"
        
        - :white_check_mark: Par exemple `<a href="https://example.com">Website</a> `
        - :x:
        - :x:
        - :x:

???+ question "3. Quel est le rôle d'un fichier css ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) Créer le contenu d'une page web
        - [ ] b) Définir l'apparence des éléments constituants une page web
        - [ ] c) Définir la structure d'une page web (titres, paragraphes, ...)
        - [ ] d) Insérer des images dans une page web

    === "Solution"
        
        - :x:
        - :white_check_mark:
        - :x:
        - :x:

???+ question "4. Les balises `<h1>, <h2>, ... <h6>`, permettent :"
  

    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) De créer des listes numérotées dans une page web
        - [ ] b) De créer des listes à puces dans une page web
        - [ ] c) De créer plusieurs niveaux de titres dans une page web
        - [ ] d) De créer des paragraphes dans une page web

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark:
        - :x:

???+ question "5. Le Web fonctionne sur le :"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) modèle client-serveur
        - [ ] b) modèle pair à pair
        - [ ] c) modèle client-client
        - [ ] d) modèle serveur-serveur

    === "Solution"
        
        - :white_check_mark:
        - :x:
        - :x:
        - :x:

???+ question "6. Quelle ligne dans le fichier css d'une page web permettra aux titres de niveau 1 de cette page de s'afficher en rouge ?"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) `h1 {color:red;}`
        - [ ] b) `title {color:red;}`
        - [ ] c) `.h1 {couleur:red;}`
        - [ ] d) `<h1> {color:red;}`

    === "Solution"
        
        - :white_check_mark:
        - :x:
        - :x:
        - :x:

???+ question "7. Quelle est la balise à utiliser pour insérer une image nommée logo_python.jpg dans une page web ?"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) `<image src="logo_python.jpg">`
        - [ ] b) `<image source="logo_python.jpg">`
        - [ ] c) `<img src="logo_python.jpg">`
        - [ ] d) `<img source="logo_python.jpg">`

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark:
        - :x:

???+ question "8. Quelles balises doit-on utiliser pour créer une liste d'éléments non ordonnées dans une page web ?"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) `<ol> et <li>`
        - [ ] b) `<ul> et <li>`
        - [ ] c) `<ol> et <il>`
        - [ ] d) `<ul> et <ol>`

    === "Solution"
        
        - :x:
        - :white_check_mark:
        - :x:
        - :x:



## II. Exercices

???+ question "1. Corriger des erreurs"

    Corriger les erreurs dans les fragments de code HTML suivant :

    ```html 
    <h> Mon titre principal </h>
    ```

    ```html
    <href="http://www.wikipedia.fr">un lien vers Wikipedia</href>
    ```

    ```html
    <p> Ce paragraphe contient un <a href="autre_page.html">lien vers une autre page</p></a>
    ```

    ??? success "Solution"

        ```html 
        <h1> Mon titre principal </h1>
        <a href="http://www.wikipedia.fr">un lien vers Wikipedia</a>
        <p> Ce paragraphe contient un <a href="autre_page.html">lien vers une autre page</a></p>
        ```




???+ question "2. Réaliser un mini-site"

    Réaliser  un mini site *Web*, en utilisant des fichiers html et **une** feuille de style CSS. Le sujet du site est au choix, par exemple : vos films préférés, un site de recette de cuisines, un site sur un sport ou une de vos passions, ou sur une célébrité (sportif, acteur, chanteur ...) . 
    
    👉 Respecter le cahier des charges suivant :

    * au moins 2 pages reliées entre elles par des liens internes.
    * au moins 2 images, attention à utiliser des images **libres de droits** ou à créer vos propres illustrations pour votre site.
    * Il y aura un lien vers un site extérieur.
    * L'apparence du site sera uniformisée (c'est à dire que d'une page à l'autre on retrouvera les mêmes couleurs et la même présentation). Vous devrez pour cela utiliser **obligatoirement** une feuille de style css dans un fichier séparé.
