---
author: Mireille Coilhac et Jean-Louis Thirot
title: Les listes de listes
---

Nous avons vu une utilisation des listes de listes dans la vidéo réalisée par Charles Poulmaire : 
[Vidéo sur les listes](https://www.lumni.fr/video/notion-de-listes-en-informatique-et-application-aux-images-numeriques#containerType=folder&containerSlug=revisions-bac-numerique-et-sciences-informatiques-1){ .md-button target="_blank" rel="noopener" }


## I. Présentation des matrices

???+ question "Une remarque pour commencer"

    Exécuter ci-dessous

    {{IDE('scripts/ecriture_liste')}}

!!! info "Accéder aux valeurs d'un tableau"

     🤔 Et oui... puisque `a` référence `[1, 2, 3]`,  cela implique qu'il revient au même d'écrire `a` ou `[1, 2, 3]` !

    Donc `a[0]` est la même chose que `[1, 2, 3][0]`

    😂 Quel intérêt ? Personne n'écrit les choses ainsi ?

    😊 Certes, mais cela pourrait vous aider à y voir clair dans ce qui suit...


!!! example "Exemple de matrice"

    Voici un exemple de matrice (dans le langage courant on parle aussi de _tableau_ au sens du tableur mais la confusion possible avec les tableaux informatiques que nous connaissons est trop risquée, nous parlerons donc de matrices)  :

     ![matrice](images/matrice.gif){ width=12% }


    Nous pouvons représenter cette matrice en Python à l'aide d'une liste... de listes (le nom de la variable **`m`** évoque le 'm' de matrice bien sûr) :

    ```python
    m = [[1, 3, 4], [5, 6, 8], [2, 1, 3]]
    ```

???+ question "À vous de jouer"

    Compléter ci-dessous : à l'aide d'une boucle `for` parcourir la liste `m` et afficher tous les éléments.

    {{IDE('scripts/parcours_m')}}


??? success "Solution"

    ```python
    for element in m :
        print(element)
    ```



## II. Accéder à une valeur    

???+ question "Accéder à une valeur"

    Comment accéder à la valeur **6** de la matrice `m` ?  
    6 est la valeur se trouvant à la 2ème ligne 2ème colonne.

    Mais regardons comment est écrite la matrice :

    `m = [[1, 3, 4], [5, 6, 8], [2, 1, 3], [7, 8, 15]]`

    La deuxième ligne est clairement : m[1], vous êtes d'accord ?
    Et du coup, 6 est la 2ème valeur de deuxieme_ligne, on est toujours d'accord ?

    Tester ci-dessous :

    {{IDE('scripts/acceder_6')}}



???+ question "À vous de jouer 3"

    Compléter ci-dessous pour faire afficher la valeur **7**.  
    Vous utiliserez la deuxième méthode de l'exemple précédent

    {{IDE('scripts/acceder_7')}}


??? success "Solution"

    ```python
    print(m[3][0])
    ```

!!! info "Accéder à un élément"

     🌵 Attention : la première ligne correspond à **`i = 0`**, et la première colonne à **`j = 0`**  

     👉 Pour accéder à la ligne **`i`**, colonne **`j`**, on ecrira donc **`m[i][j]`**  
     
     😊 Il suffit de le savoir, alors maintenant... vous savez ! 


!!! info "Présentation"

    Il est souvent plus pratique de présenter ces "tableaux de tableaux" comme suit :

    ```python
    m = [[1, 3, 4],
        [5, 6, 8],
        [2, 1, 3],
        [7, 8, 15]]
    ```


    Nous obtenons ainsi quelque chose qui ressemble beaucoup à un "objet mathématique" très utilisé : une matrice


???+ question "À vous de jouer 4"

    Ecrire ci-dessous toutes les instructions qui affichent **8**.

    {{IDE('scripts/acceder_8')}}


??? success "Solution"

    ```python
    print(m[1][2])
    print(m[3][1])
    ```

???+ question "À vous de jouer 5"

    Compléter la fonction `accede_valeur` qui prend en paramètres un tableau `tableau`, et deux entiers `ligne` et `colonne`.  
    Cette fonction doit renvoyer l'élément de `tableau`, situé à la ligne `ligne` et à la colonne `colonne`.

    {{IDE('scripts/acceder_valeur')}}

## III. Parcourir une matrice

!!! info "Un double boucle for"

    Pour parcourir les éléments d'une matrice on va utiliser une "double boucle for".

???+ question "À vous de jouer 6"

    Compléter ci-dessous pour obtenir l'affichage suivant :

    ```pycon
    la valeur aux indices ligne = 0 , colonne = 0  est  1
    la valeur aux indices ligne = 0 , colonne = 1  est  3
    la valeur aux indices ligne = 0 , colonne = 2  est  4
    la valeur aux indices ligne = 1 , colonne = 0  est  5
    la valeur aux indices ligne = 1 , colonne = 1  est  6
    la valeur aux indices ligne = 1 , colonne = 2  est  8
    la valeur aux indices ligne = 2 , colonne = 0  est  2
    la valeur aux indices ligne = 2 , colonne = 1  est  1
    la valeur aux indices ligne = 2 , colonne = 2  est  3
    la valeur aux indices ligne = 3 , colonne = 0  est  7
    la valeur aux indices ligne = 3 , colonne = 1  est  5
    la valeur aux indices ligne = 3 , colonne = 2  est  1
    ```

    {{IDE('scripts/afficher_m_tout')}}


??? success "Solution"

    ```python
    m = [[1, 3, 4], 
        [5, 6, 8], 
        [2, 1, 3],
        [7, 5, 1]]
        
    nb_colonnes = 3
    nb_lignes = 4
    for i in range(nb_lignes):
        for j in range(nb_colonnes):
            valeur = m[i][j]
            print("la valeur aux indices ligne =", i,", colonne =", j," est ", valeur)
    ```

???+ question "À vous de jouer 7 : parcours par lignes"

    Ecrire la fontion `get_ligne`qui prend en paramètres une matrice `lst`, et un entier `i`.  
    Cette fonction doit renvoyer la ligne `i` de la matrice `lst`.

    {{IDE('scripts/parcours_ligne')}}


???+ question "À vous de jouer 8 : parcours par colonnes"

    Compléter la fonction `get_colonne` qui prend en paramètres une matrice `matrice`, un entier `colonne` et renvoie la liste des éléments de la colonne `colonne`

    Par exemple :

    ```pycon
    >>> m = [[1, 3, 4],
        [5, 6, 8],
        [2, 1, 3],
        [7, 8, 15]]
    >>> get_colonne(m, 0)
    [1, 5, 2, 7]
    >>> get_colonne(m, 2)
    [4, 8, 3, 15]
    >>> 
    ```

    {{IDE('scripts/parcours_colonne')}}


_Auteurs : Mireille Coilhac et Jean-Louis Thirot_
