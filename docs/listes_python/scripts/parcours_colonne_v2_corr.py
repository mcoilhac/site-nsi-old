def donne_colonne(matrice, n_colonne) -> list :
    """
    Entrée : matrice : une liste de listes, un entier n_colonne le numéro de la colonne
    Sortie : colonne : une liste dont les éléments sont les éléments de la colonne numéro n_colonne
    """
    colonne = [matrice[ligne][n_colonne] for ligne in range(len(matrice))]
    return colonne

    