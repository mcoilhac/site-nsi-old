def get_ligne(lst, i) -> list :
    """
    Entrées : 
      - lst : une liste de listes
      - i :un entier
    Sortie : une liste dont les éléments sont les éléments de la ligne i
    """
    return lst[i]
    