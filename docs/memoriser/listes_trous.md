---
author: Nicolas Revéret et Mireille Coilhac
title: Listes - À vous
---


!!! abstract "Tableau"

    En informatique un **tableau** :
    
    - est une structure de données linéaire dans laquelle des **éléments** sont repérés par leur **indice**,
  
    - contient des éléments de même type,
  
    - est de taille *fixe*.

!!! abstract "Listes Python"

    En Python on utilise le type `#!py list` pour représenter les tableaux.

    On peut **accéder** à un élément en faisant `#!py tableau[indice]`.

    Important : le premier élément d'un tableau correspond à l'indice ... .

    Par exemple : pour `notes = [18, 15, 16]` on accède à la note 18 avec `notes[...]`, et à la note 15 avec `notes[...]`
    
    On peut **modifier** un élément en faisant `#!py tableau[indice] = nouvel_element`.

    La **longueur** du tableau est donnée par ...


!!! abstract "Parcours"

    Python permet de parcourir les tableaux, comme `#!py neveux = ["Riri", "Fifi", "Loulou"]` :

    * selon les indices :

        ```pythonon
        for i in range(len(neveux)):
        	print("L'élément d'indice", i, "est", neveux[i])
        ```

        Ce script produira l'affichage suivant en console :

        ```pycon
        ...
        ...
        ...
        >>>
        ```
    
    * selon les valeurs :

        ```python
        for neveu in neveux:
        	print(neveu, "est un élément")
        ```
        Ce script produira l'affichage suivant en console :

        ```pycon
        ...
        ...
        ...
        >>>
        ```

!!! abstract "Échanger des valeurs"

    Pour échanger des valeurs d'un `tableau`, on peut :

    * utiliser une variable tierce :

        ```python
        temporaire = ...
        tableau[i] = ...
        tableau[j] = ...
        ```
    
    * utiliser l'affectation multiple qui se fait de façon **simultanée**:
        
        ```python
        ...
        ```

!!! abstract "Liste en compréhension"

	```pycon
	>>> liste_1 = [9*i for i in range (1, 11)]
	>>> liste_1
	...
	>>> liste_2 = [9*i for i in range (1, 11) if 9*i % 2 == 0]
	>>> liste_2
	...
	```

!!! abstract "Listes de listes"

	```pycon
	>>> ma_matrice = [["a", "b", "c"], ["d", "e", "f"], ["g", "h", "i"]]
	>>> ma_matrice[1]
	...
	>>> ma_matrice[1][0]
	...
	```







