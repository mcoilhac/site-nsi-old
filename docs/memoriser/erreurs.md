---
author: Mireille Coilhac
title: Erreurs - Bilan
---

???+ note dépliée "Les messages d'erreurs"

    Les messages d'erreurs ont difficiles à lire. Il faut néanmoins essayer de les comprendre 😂.

## `ValueError`

!!! info "ValueError"

    on utilise une valeur non valide dans une expression.


!!! example "Exemple"

    ```pycon
    >>> int("Albert")
	Traceback (most recent call last):
  	File "<interactive input>", line 1, in <module>
	ValueError: invalid literal for int() with base 10: 'Albert'
	```

## `SyntaxError`

!!! info "SyntaxError"

	Cette erreur, lors de l'exécution, est fréquente. Par exemple il manque une parenthèse, ou toute autre syntaxe erronée (nombreux cas possibles)

	👉 Vous devez lire les messages d'erreurs et apprendre à les comprendre pour être en mesure de corriger vos codes.

!!! example "Exemple"

    ```python
    print("Bonjour"
	```

	Si on exécute ce code on obtient : 

	```pycon
	SyntaxError: unexpected EOF while parsing
	```

## `NameError`

!!! info "SyntaxError"

	Erreur également très fréquente, vous devez absolument réagir sans hésiter : une variable est utilisée mais n'a pas été définie.


!!! example "Exemple"

	```pycon
	>>> print(a)
	Traceback (most recent call last):
  	  File "<interactive input>", line 1, in <module>
	NameError: name 'a' is not defined
	```
	Cela signifie que vous demandez d'afficher la valeur de `a`, mais il n'y a pas de variable nommée `a` dans votre code.

## `TypeError`

!!! info "TypeError"

	On a fait une opération entre 2 types incompatibles


!!! example "Exemple"

	```pycon
	>>> 1 + "Albert"
	Traceback (most recent call last):
	  File "<interactive input>", line 1, in <module>
	TypeError: unsupported operand type(s) for +: 'int' and 'str'
	```

## `IndentationError`

!!! info "`IndentationError`"

	Erreur d'indentation

!!! example "Exemples"

	* `unexcpected indent` : Une ligne du code est indentée mais ne devrait pas (elle n'est pas dans un nouveau bloc)
	* `expected an indented block` : Une ligne n'est pas indentée et elle devrait. Le plus souvent vous avez simplement oublié de l'indenter.
	* `indent does not match any outer indentation level` : Une ligne n'est pas indentée comme celles qui précède (souvent un espace en plus ou en moins)


