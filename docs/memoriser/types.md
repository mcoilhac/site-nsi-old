---
author: Mireille Coilhac
title: Types de variables - Bilan
---

## Types de base

|Type|Description|Exemples
|:--|:--|:--|
|`int`|Les nombres entiers|`ma_variable = 3`|
|`float`|Les nombres à virgule|`ma_variable = 2.5`|
|`str`|Les chaînes de caractères|`ma_variable = "NSI"`|
|`bool`|Les booléens|`ma_variable = True`|


!!! warning "Remarque"

    Nous étudierons plus tard dans l'année le type liste Python `list`, et le type dictionnaire `dict`

!!! info "Les flottants"

	1 est un entier, et 1.0 est un `float`. Ils sont égaux (ces deux écritures représentent la même valeur) mais pas identiques :

	```pycon
	>>> 1 == 1.0
	True
	>>> 1 is 1.0
	False
	>>> type(1)
	<class 'int'>
	>>> type(1.0)
	<class 'float'>
	```



## Conversion d'un type à un autre

!!! info "Les transtypages"

	* On peut convertir des `float` en `int`, ou inversement. 

	* On peut également convertir des chaînes `str` en `int` ou en `float` mais seulement si la chaîne contient un nombre compréhensible.

	```python
	a = int(1.23)   # a vaudra 1
	a = float(1)    # a vaudra 1.0
	a = int("12")   # a vaudra 12 car on a converti la chaine "12" en entier qui vaut 12
	chaine = str(3) # chaine vaudra "3" car on a converti l'entier 3 en str
	```

	😰 **Mais**

	```python
	a = int("douze")  # TValueError : int ne sait pas interpréter douze comme un nombre               
	a = int("12.3")   # ValueError : idem, int ne sait pas interpréter 12.3 comme un entier
	a = int("1.0")    # ValueError : idem, int ne sait pas interpréter 1.0 comme un entier
	```

	


