---
author: Mireille Coilhac
title: Téléchargement des fiches à compléter
---

[Outils](a_telecharger/outils_2023.pdf){ .md-button target="_blank" rel="noopener" }

[Logique](a_telecharger/logique_trous_2023.pdf){ .md-button target="_blank" rel="noopener" }

[Binaire](a_telecharger/binaire_trous_2023.pdf){ .md-button target="_blank" rel="noopener" }

[Vocabulaire de base](a_telecharger/voc_base_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Algorithme d'échange](a_telecharger/echanges_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Les types de variables](a_telecharger/types_trous_2023.pdf){ .md-button target="_blank" rel="noopener" }

[input et print](a_telecharger/input_print_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Opérateurs](a_telecharger/operateurs_trous.pdf){ .md-button target="_blank" rel="noopener" }

[if ... elif ... else](a_telecharger/if_elif_else_bilan.pdf){ .md-button target="_blank" rel="noopener" }

[Les erreurs](a_telecharger/erreurs.pdf){ .md-button target="_blank" rel="noopener" }

[La boucle `for`](a_telecharger/for_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Compteurs et accumulateurs](a_telecharger/cpt_accus.pdf){ .md-button target="_blank" rel="noopener" }

[La boucle `while`](a_telecharger/while_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Les fonctions](a_telecharger/fonctions_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Les listes](a_telecharger/listes_tableaux_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Les tuples](a_telecharger/tuples_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Méthodes de listes](a_telecharger/methodes_listes_trous.pdf){ .md-button target="_blank" rel="noopener" }

[Dictionnaires](a_telecharger/dicos_trous.pdf){ .md-button target="_blank" rel="noopener" }




    