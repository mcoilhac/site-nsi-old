---
author: Jean-Louis Thirot, Gilles Lassus et Mireille Coilhac
title: While - À vous
---

## I. Syntaxe et utilisation

!!! info "Syntaxe générale"

	```python
	while condition:
	    instructions  # (1)
	```

	1. bloc d’instructions qui sera exécuté **tant que** `condition` est `True`.


!!! info "Utilisation"

	La boucle `while` est utile ... 

	Elle est utilisée pour traiter des problèmes tels que :  
    > 👉 Faire quelque chose ...


!!! warning "Ne pas en abuser"

    Le code ci-dessous, pour afficher les entiers de 0 à `n` est correct mais **à éviter** :

    ```python
    i = 0
	while i < n + 1 :
    	print(i)
    	i = i + 1
    ```

    Il y a un risque d'oublier d'incrémenter `i` dans la boucle... le code ne se terminerait pas (et c'est très agaçant).

!!! info "Boucle `for`"

	Quand on sait à l'avance combien de fois une boucle doit être exécutée, on utilise **de préférence** une boucle `for`.


!!! info "Ou jusqu’à ce que ????"

	Le problème est parfois proposé de façon un peu différente : faire quelques chose jusqu'à ce que...  
	

	Par exemple, on demande un mot de passe, et on répète jusqu'à ce que le mot de passe soit le bon.
	Cet exemple est équivalent à celui-ci : 
	> On demande un mot de passe, et on répète  tant que ...

	👉 Dans le 1er cas l'énoncé du problème indique la condition d'arrêt.
	
	👉 dans le second cas il indique la condition de continuation. 
	
	😊 A vous de reformuler avec la bonne condition...

!!! info "Initialisation"

	Pour exécuter l'instruction `while condition` : il faut que la condition soit un booléen dont la valeur est définie.

	Dans l'exemple suivant, Pour évaluer l'expression `mot_de_passe != "123456"` il faut que la variable `mot_de_passe` ait été initialisée.

	```python
	mot_de_passe = ...  # (1)
	while mot_de_passe != "123456" :
    	mot_de_passe = input("Entrez le mot de passe : ")
    ```

    1. Initialisation de `mot_de_passe`

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"

## II. Les pièges

😰 Il est fréquent d'oublier une ligne de code, et d'écrire une boucle while qui ne se termine pas.

!!! bug "Boucle **infinie**"

    ```python
    n = 0
	while n < 10 :
   		print(n)
   	```

!!! info "Version correcte"

	On incrémente `n` dans la boucle, `n` finira donc par atteindre la valeur d'arrêt :

	```python
    n = 0
	while n < 10 :
   		print(n)
   		...
   	```

!!! bug "Ne **jamais entrer** dans la boucle"

	```python
	a = 0
	while a > 10:
    	print("Bonjour !")  # (1)
    	a = a + 1

	print("Au revoir !")  # (2)
	```

	1. Ce texte ne s'affichera jamais car on ne rentre pas dans la boucle `while`

	2. Il s'affichera directement ce texte.


## III. Les drapeaux ou flags

!!! info "flag"

	Un drapeau (flag en anglais) est une variable qui sert à marquer une situation. 

	Par exemple, dans un jeu, on continue de jouer tant que la partie n'est pas finie. 
 
 	👉 On pourra créer un flag booléen `partie_en_cours` :

	```python
	partie_en_cours = True  # (1)
	while partie_en_cours :  # (2)
    	bloc d instructions du jeux

    	# quelque part dans ce bloc on aura :
    	if quelque chose qui arrête le jeu :
        	partie_en_cours = False  # (3)
	```

	1. Le drapeau (ou flag)
	
	2. `partie_en_cours` est un booléen qui vaut `True` ou `False`. On rentre dans la boucle si ce booléen est évalué à `True`. Il est donc une mauvaise pratique d'écrire `while partie_en_cours == True`

	3. Le drapeau (flag) passe à `False`, on sort donc de la boucle `while`

