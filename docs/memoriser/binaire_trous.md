---
author: Mireille Coilhac
title: Binaire - À vous
---

## Prérequis sur les puissances

![puissances](images/regles_puissances.jpg){ width=15%; : .center }

???+ question "QCM à refaire tant que nécessaire ..."

	Question 1 : $2^3=$

	{{ qcm(["9", "8", "Je ne sais pas", "6"], [2], shuffle = True) }}


	Question 2 : $3^2=$

	{{ qcm(["9", "8", "Je ne sais pas", "6"], [1], shuffle = True) }}


	Question 3 : $2^0=$

	{{ qcm(["0", "1", "Impossible", "2"], [2], shuffle = True) }}


	Question 4 : $2^1=$

	{{ qcm(["0", "1", "Je ne sais pas", "2"], [4], shuffle = True) }}


	Question 5 : $0^3=$

	{{ qcm(["0", "1", "Impossible", "3"], [1], shuffle = True) }}


	Question 6 : $16^3 \times 16^2=$

	{{ qcm(["$16^5$", "$16^6$", "Je ne sais pas", "2"], [1], shuffle = True) }}


	Question 7 : $16^0 \times 16^2=$

	{{ qcm(["$16^0$", "$16^2$", "Je ne sais pas", "1", "16"], [2], shuffle = True) }}


	Question 8 : $2^3+2^4=$

	{{ qcm(["$2^7$", "$2^{12}$", "Je ne sais pas", "24"], [4], shuffle = True) }}


## Les bases numériques


!!! info "Numération de position"

	C’est celle que nous utilisons actuellement tous les jours :

	$2023 = 2 \times 1000 +$ ...

	Ou encore : 2023 est égal à 2 **milliers**, 0 $\hspace{5em}$, 2 $\hspace{5em}$, 3 $\hspace{5em}$.

	$2023 = 2 \times 10^3 +$ ...


### Lire un nombre

!!! example "un exemple en base 10"

	$12734 = 4 \times 10^0 + 3 \times 10^1 + 7 \times 10^2 + 2 \times 10^3 + 1 \times 10^4$


!!! example "un exemple en base 2"

	$(101010)_2 =$ ...


😊 Pas si difficile ! Vous avez appris à convertir un nombre d'une base N vers la base 10.


!!! info "Notations"

	Pour préciser dans quelle base sont écrits les nombres, on peut utiliser des parenthèses : 

	$(42)_{10}=(101010)_2$

	Autres notations possibles :$(101010)_2 = 101010 _{(2)}= 0\text{b}101010 = 101010\text{b}$


### Compter en binaire

!!! example "Quelques exemples"

	$(1)_2+(1)_2=(2^0  + 2^0)_{10}=(2 \times 2^0)_{10} =(2^1)_{10}= (10)_2$

	$(10)_2 +(1)_2=(2^1+2^0)_{10}=(11)_2$

	$(11)_2+(1)_2= (2^1+2^0+2^0)_{10}=  (2^1+2 \times 2^0)_{10}=  (2^1+2^1)_{10}=(2 \times 2^1)_{10}=(2^2)_{10}=(100)_2$


!!! info "Comptons en base 2"

	Compter, c'est ... ajouter 1 😊 

	0; 1; 10; 11; 100; 101; 110; ...


???+ question "Si on comptait ?"

    Trouver les entiers qui suivent ceux-ci lorsque l'on compte : 0; 1; 10; 11; 100; 101; 110;

    $\hspace{5em}$

    $\hspace{5em}$


???+ note dépliée "Remarquer en décimal"

	* 99 + 1 = ... 
	* 999 + 1 = ...
	* 9999 + 1 = ...

### De la base 10 vers la base 2

!!! info "Poser les divisions _à la main_"

	Petit retour sur les divisions dites "euclidiennes"

	![div_eucl](images/div_eucl.png){ width=20%; : .center }

	> Source de l'image : [🌐 Division euclidienne](https://www.jeuxmaths.fr/cours/division-euclidienne.php){:target="_blank" }


!!! info "Méthode par divisions successives"

	🤔 Etudier l'exemple ci-dessous

	👉 On poursuit les divisions jusqu'à obtenir un quotient **égal à 0** pour la dernière division.  
	On lit ensuite les restes, en partant du bas.

	![decimal_binaire](images/decimal_binaire.png){ width=30%; : .center }

	> Source de l'image : [🌐 Académie de Limoges](http://pedagogie.ac-limoges.fr/sti_si/accueil/FichesConnaissances/Sequence2SSi/co/ConvDecimalBinaire.html){:target="_blank" }



## Exercices

???+ question "Exercice 1"

    1110010 est écrit en base 2. L'écrire en base 10.

    $\hspace{5em}$

    $\hspace{5em}$



???+ question "Exercice 2"

    convertir 23 écrit en décimal en binaire

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$


???+ question "Exercice 3"

    234 est écrit en base 10. L'écrire en binaire

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$


## Des opérations en binaire

### Les additions

!!! info "Rappel en décimal"

    ![addition décimale](images/addition_dec.png){ width=20% }

???+ question "Additionner _à la main_"

    $1100110_2+1111101_2 = ?$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

???+ question "Additionner _à la main_"

    $1111111_2+1_2 = ?$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    $\hspace{5em}$

    On a donc $1111111_2+1_2$ = ...

	👉 C'est important à remarquer, et cela se généralise évidemment pour tous les nombres constitués uniquement de 1 auxquels on ajoute 1.



### Les multiplications

!!! info "💚 A noter"

    Quand on multiplie en binaire par **2** on écrit un $\hspace{15em}$, comme pour la multiplication par **10** en décimal.


## Les bits et les octets

???+ question "Exercice 1"

    Pour coder tous les nombres entiers de 1 à 1000, combien de bits faut-il ?

    $\hspace{5em}$

    $\hspace{5em}$

!!! info "Les octets"

	Dans la mémoire de l'ordinateur les informations sont codées dans des octets. C'est la plus petite unité d'information qu'on peut lire/écrire. Un octet correspond à ... bits.

	👉 Un octet permet donc de coder $\hspace{5em}$ valeurs différentes.

