---
author: Mireille Coilhac
title: Exercices sur les chaines de caractères
---

## Prérequis : bases de Python

[Filtre par longueur](https://codex.forge.apps.education.fr/exercices/liste_longueurs/){ .md-button target="_blank" rel="noopener" }

[Compte d'occurences](https://codex.forge.apps.education.fr/exercices/cpt_occurrences/){ .md-button target="_blank" rel="noopener" }

[Dentiste](https://codex.forge.apps.education.fr/exercices/dentiste/){ .md-button target="_blank" rel="noopener" }

[Renverser une chaine](https://codex.forge.apps.education.fr/exercices/renverse_chaine/){ .md-button target="_blank" rel="noopener" }

[Anonymat 1](https://codex.forge.apps.education.fr/exercices/anonymat_1/){ .md-button target="_blank" rel="noopener" }

[Anniversaire de chat](https://codex.forge.apps.education.fr/exercices/channiv/){ .md-button target="_blank" rel="noopener" }

## Prérequis : dictionnaires

[mra_bs_clguba](https://codex.forge.apps.education.fr/en_travaux/mra_bs_clguba/){ .md-button target="_blank" rel="noopener" }