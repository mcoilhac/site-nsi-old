---
author: Mireille Coilhac
title: 🏡 Accueil
---


# Bienvenue sur la page de NSI des classes de 1ère du Lycée Saint-Aspais de MELUN

Cours de première NSI (classes de Mme Coilhac)

😊 Ce site évoluera pendant l'année ...

!!! info "Exercices Python : Utilisation de l'IDE sur le site"


    Pour la plupart des exercices il y a un IDE possédant 5 boutons dont les 2 principaux sont :

    <div class="py_mk_ide">
    <ul>
    <li> <button class="tooltip"><img src="pyodide-mkdocs/icons8-play-64.png"></button> pour <b>Lancer</b> le script (l'exécuter). 

    <li> <button class="tooltip"><img src="pyodide-mkdocs/icons8-check-64.png"></button> pour <b>Valider</b> votre code. <b>Il faut absolument cliquer sur ce bouton</b>, car il se peut que l'exécution ne pose pas de problème avec le bouton <b>Lancer</b> alors que votre code 
    ne répond pas à tout ce qui est demandé dans l'exercice. 
    <br> Votre code est évalué à l'aide de tests cachés. Si tous les tests cachés sont réussis, le corrigé et éventuellement des remarques, s'affichent. 
    <br> ⚠ Parfois ce bouton n'existe pas, lorsqu'il faut juste exécuter un script, et qu'aucune validation n'est prévue.

    </ul>
    </div>


    ??? tip "Si vous voulez vraiment voir le corrigé"

        <div class="py_mk_ide">

        Si vous voulez vraiment voir le corrigé parce que vous êtes bloqué, il faut mettre en commentaire les <code>assert</code>: <br>
        Cliquer sur <b><code>###</code></b> qui se trouve en haut à droite de l'éditeur. À chaque fois que vous effectuerez un **Lancer** avec <button class="tooltip"><img src="pyodide-mkdocs/icons8-play-64.png"></button> suivi d'une validation avec <button class="tooltip"><img src="pyodide-mkdocs/icons8-check-64.png"></button> l'essai sera décompté en bas à droite dans "Evaluations restantes". Lorsqu'il y a 0 évaluation restante, la solution s'affiche, suivie parfois d'une remarque.

        </div>
 

    Les autres boutons sont :


    <div class="py_mk_ide">
    <ul>
    <li> Télécharger le script actuel : <button class="tooltip"><img src="pyodide-mkdocs/icons8-download-64.png"></button></li>

    <li> Téléverser un script local : <button class="tooltip"><img src="pyodide-mkdocs/icons8-upload-64.png"></button></li>

    <li> Recharger l'énoncé : <button class="tooltip"><img src="pyodide-mkdocs/icons8-restart-64.png"></button></li>

    <li> Sauvegarder en ligne le script actuel : <button class="tooltip"><img src="pyodide-mkdocs/icons8-save-64.png"></button></li>
    <br> Après avoir cliqué sur ce bouton, si vous changez de page, ou rafraichissez la page dans votre navigateur, vous retrouverez ce script. Cela ne permet pas de télécharger votre script (bouton <button class="tooltip"><img src="pyodide-mkdocs/icons8-download-64.png"></button> pour cela)
    
    </ul>
    </div>


